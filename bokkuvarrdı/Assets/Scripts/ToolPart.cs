﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "ToolPart", menuName = "", order = 1)]
public class ToolPart : ScriptableObject
{
    public Sprite partSprite;
    public String partName;
    public PartType type;
    public ConnectionType[] connectionPoints;
    public UnityEvent action;
}


public enum PartType
{
    Control,
    Consumption,
    Ammunition,
    Vision,
    Additional

}


public enum ConnectionType
{
    X,O,R,T,E,F
}
